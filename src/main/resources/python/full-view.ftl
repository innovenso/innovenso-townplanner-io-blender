import bpy

<#include "cleanup.ftl">

bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="Cameras", active_collection=False)
bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="Scenery", active_collection=False)
bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="View", active_collection=False)

topTitle = bpy.data.objects["View Title"]
topTitle.data.body = "${element.title}"

description = bpy.data.objects["View Description"]
description.data.body = "${element.description}"

capabilityCollection=bpy.data.collections["Business Capabilities"]

<#list element.businessCapabilities as capability>
bpy.ops.wm.link(directory="${capability.blenderFilePath}\\Collection\\", filename="Business Capability", relative_path=True, active_collection=False)
capability=bpy.data.objects["Business Capability"]
capability.name="${capability.label}"
capability.location=(${capability?index*6},${capability.level*6-36},0)
capabilityCollection.objects.link(capability)
bpy.context.scene.collection.objects.unlink(capability)
</#list>

buildingBlockCollection=bpy.data.collections["Building Blocks"]

<#list element.buildingBlocks as buildingBlock>
bpy.ops.wm.link(directory="${buildingBlock.blenderFilePath}\\Collection\\", filename="Building Block", relative_path=True, active_collection=False)
buildingBlock=bpy.data.objects["Building Block"]
buildingBlock.name="${buildingBlock.label}"
buildingBlock.location=(${buildingBlock?index*3},-15,0)
buildingBlockCollection.objects.link(buildingBlock)
bpy.context.scene.collection.objects.unlink(buildingBlock)
</#list>

capabilityBuildingBlockMappingCollection=bpy.data.collections["Capability Building Block Mappings"]

<#list element.capabilityBuildingBlockMappings as connection>
bpy.ops.wm.append(directory="${connection.blenderFilePath}\\Collection\\", filename="Connection", active_collection=False)
connection=bpy.data.objects["Connection"]
connectionGroup=bpy.data.collections["Connection"]
connectionLine=bpy.data.objects["Connection Line"]
connectionOrigin=bpy.data.objects["Connection Origin"]
connectionTarget=bpy.data.objects["Connection Target"]
connectionTitle=bpy.data.objects["Connection Title"]
source=bpy.data.objects["${connection.buildingBlock}"]
target=bpy.data.objects["${connection.capability}"]
connectionOrigin.parent=source
connectionTarget.parent=target
connection.name="${connection.label}"
connectionGroup.name="${connection.label}"
connectionLine.name="${connection.label} Line"
connectionOrigin.name="${connection.label} Origin"
connectionTarget.name="${connection.label} Target"
connectionTitle.name="${connection.label} Title"
connection.location=(${connection?index*3},-30,0)
capabilityBuildingBlockMappingCollection.children.link(connectionGroup)
bpy.context.scene.collection.children.unlink(connectionGroup)
</#list>

systemCollection=bpy.data.collections["Systems"]

<#list element.systems as system>
bpy.ops.wm.link(directory="${system.blenderFilePath}\\Collection\\", filename="System", relative_path=True, active_collection=False)
system=bpy.data.objects["System"]
system.name="${system.label}"
system.location=(${system?index*3},-3,0)
systemCollection.objects.link(system)
bpy.context.scene.collection.objects.unlink(system)
</#list>

buildingBlockSystemMappingCollection=bpy.data.collections["Building Block System Mappings"]

<#list element.buildingBlockSystemMappings as connection>
bpy.ops.wm.append(directory="${connection.blenderFilePath}\\Collection\\", filename="Connection", active_collection=False)
connection=bpy.data.objects["Connection"]
connectionGroup=bpy.data.collections["Connection"]
connectionLine=bpy.data.objects["Connection Line"]
connectionOrigin=bpy.data.objects["Connection Origin"]
connectionTarget=bpy.data.objects["Connection Target"]
connectionTitle=bpy.data.objects["Connection Title"]
source=bpy.data.objects["${connection.system}"]
target=bpy.data.objects["${connection.buildingBlock}"]
connectionOrigin.parent=source
connectionTarget.parent=target
connection.name="${connection.label}"
connectionGroup.name="${connection.label}"
connectionLine.name="${connection.label} Line"
connectionOrigin.name="${connection.label} Origin"
connectionTarget.name="${connection.label} Target"
connectionTitle.name="${connection.label} Title"
connection.location=(${connection?index*3},-15,0)
buildingBlockSystemMappingCollection.children.link(connectionGroup)
bpy.context.scene.collection.children.unlink(connectionGroup)
</#list>

systemContextCollection=bpy.data.collections["System Contexts"]

<#list element.systemContexts as system>
bpy.ops.wm.link(directory="${system.blenderFilePath}\\Collection\\", filename="System Context", relative_path=True, active_collection=False)
system=bpy.data.objects["System Context"]
system.name="${system.label} context"
system.location=(${system?index*3},-9,0)
systemContextCollection.objects.link(system)
bpy.context.scene.collection.objects.unlink(system)
</#list>

containerCollection=bpy.data.collections["Containers"]

<#list element.containers as container>
bpy.ops.wm.link(directory="${container.blenderFilePath}\\Collection\\", filename="Container", relative_path=True, active_collection=False)
container=bpy.data.objects["Container"]
container.name="${container.label}"
container.location=(${container?index*3},0,0)
containerCollection.objects.link(container)
bpy.context.scene.collection.objects.unlink(container)
</#list>

connectionCollection=bpy.data.collections["IT Connections"]

<#list element.itConnections as connection>
bpy.ops.wm.append(directory="${connection.blenderFilePath}\\Collection\\", filename="Connection", active_collection=False)
connection=bpy.data.objects["Connection"]
connectionGroup=bpy.data.collections["Connection"]
connectionLine=bpy.data.objects["Connection Line"]
connectionOrigin=bpy.data.objects["Connection Origin"]
connectionTarget=bpy.data.objects["Connection Target"]
connectionTitle=bpy.data.objects["Connection Title"]
source=bpy.data.objects["${connection.source}"]
target=bpy.data.objects["${connection.target}"]
connectionOrigin.parent=source
connectionTarget.parent=target
connection.name="${connection.label}"
connectionGroup.name="${connection.label}"
connectionLine.name="${connection.label} Line"
connectionOrigin.name="${connection.label} Origin"
connectionTarget.name="${connection.label} Target"
connectionTitle.name="${connection.label} Title"
connection.location=(${connection?index*3},-30,0)
connectionCollection.children.link(connectionGroup)
bpy.context.scene.collection.children.unlink(connectionGroup)
</#list>

actorCollection=bpy.data.collections["Actors"]

<#list element.actors as actor>
bpy.ops.wm.link(directory="${actor.blenderFilePath}\\Collection\\", filename="User", relative_path=True, active_collection=False)
actor=bpy.data.objects["User"]
actor.name="${actor.label}"
actor.location=(${actor?index*3},3,0)
actorCollection.objects.link(actor)
bpy.context.scene.collection.objects.unlink(actor)
</#list>

actorConnectionCollection=bpy.data.collections["Actor Connections"]

<#list element.actorConnections as connection>
bpy.ops.wm.append(directory="${connection.blenderFilePath}\\Collection\\", filename="Connection", active_collection=False)
connection=bpy.data.objects["Connection"]
connectionGroup=bpy.data.collections["Connection"]
connectionLine=bpy.data.objects["Connection Line"]
connectionOrigin=bpy.data.objects["Connection Origin"]
connectionTarget=bpy.data.objects["Connection Target"]
connectionTitle=bpy.data.objects["Connection Title"]
source=bpy.data.objects["${connection.actor}"]
target=bpy.data.objects["${connection.target}"]
connectionOrigin.parent=source
connectionTarget.parent=target
connection.name="${connection.label}"
connectionGroup.name="${connection.label}"
connectionLine.name="${connection.label} Line"
connectionOrigin.name="${connection.label} Origin"
connectionTarget.name="${connection.label} Target"
connectionTitle.name="${connection.label} Title"
connection.location=(${connection?index*3},-30,0)
actorConnectionCollection.children.link(connectionGroup)
bpy.context.scene.collection.children.unlink(connectionGroup)
</#list>

entityCollection=bpy.data.collections["Entities"]

<#list element.entities as entity>
bpy.ops.wm.link(directory="${entity.blenderFilePath}\\Collection\\", filename="Entity", relative_path=True, active_collection=False)
entity=bpy.data.objects["Entity"]
entity.name="${entity.label}"
entity.location=(${entity?index*3},6,0)
entityCollection.objects.link(entity)
bpy.context.scene.collection.objects.unlink(entity)
</#list>



bpy.ops.wm.save_as_mainfile(filepath="${targetFile}")