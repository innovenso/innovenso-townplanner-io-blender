import bpy

<#include "cleanup.ftl">

bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="Enterprise")

enterpriseTitle = bpy.data.objects["Enterprise Title"]
enterpriseTitle.data.body = "${element.title}"

description = bpy.data.objects["Enterprise Description"]
description.data.body = "${element.description}"

bpy.ops.wm.save_as_mainfile(filepath="${targetFile}")