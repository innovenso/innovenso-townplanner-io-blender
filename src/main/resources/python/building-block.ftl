import bpy

<#include "cleanup.ftl">

bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="Building Block")

topTitle = bpy.data.objects["Building Block Title"]
topTitle.data.body = "${element.title}"

description = bpy.data.objects["Building Block Description"]
description.data.body = "${element.description}"

bpy.ops.wm.save_as_mainfile(filepath="${targetFile}")