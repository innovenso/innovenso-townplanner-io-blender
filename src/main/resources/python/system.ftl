import bpy

<#include "cleanup.ftl">

bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="System")
bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="System Context")

systemTitle = bpy.data.objects["System Title"]
systemTitle.data.body = "${element.title}"

systemContextTitle = bpy.data.objects["System Context Title"]
systemContextTitle.data.body = "${element.title}"

description = bpy.data.objects["System Description"]
description.data.body = "${element.description}"

bpy.ops.wm.save_as_mainfile(filepath="${targetFile}")