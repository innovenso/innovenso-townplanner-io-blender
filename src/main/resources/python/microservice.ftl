import bpy

<#include "cleanup.ftl">

bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="Container")

topTitle = bpy.data.objects["Microservice Title Top"]
topTitle.data.body = "${element.title}"

sideTitle = bpy.data.objects["Microservice Title Side"]
sideTitle.data.body = "${element.title}"

description = bpy.data.objects["Microservice Description"]
description.data.body = "${element.description}"

<#include "technology-map.ftl">

bpy.ops.wm.save_as_mainfile(filepath="${targetFile}")