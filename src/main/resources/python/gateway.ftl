import bpy

<#include "cleanup.ftl">

bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="Container")

topTitle = bpy.data.objects["Gateway Title"]
topTitle.data.body = "${element.title}"

description = bpy.data.objects["Gateway Description"]
description.data.body = "${element.description}"

<#include "technology-map.ftl">

bpy.ops.wm.save_as_mainfile(filepath="${targetFile}")