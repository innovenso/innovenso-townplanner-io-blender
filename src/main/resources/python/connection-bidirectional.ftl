import bpy

<#include "cleanup.ftl">

bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="Connection")

topTitle = bpy.data.objects["Connection Title"]
topTitle.data.body = "${element.title}"

bpy.ops.wm.save_as_mainfile(filepath="${targetFile}")