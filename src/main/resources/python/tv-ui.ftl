import bpy

<#include "cleanup.ftl">

bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="Container")

topTitle = bpy.data.objects["Smart TV UI Title"]
topTitle.data.body = "${element.title}"

description = bpy.data.objects["Smart TV UI Description"]
description.data.body = "${element.description}"

<#include "technology-map.ftl">

bpy.ops.wm.save_as_mainfile(filepath="${targetFile}")