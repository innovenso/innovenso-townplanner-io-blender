<#list element.concept.technologies as technology>

if bpy.data.objects.get("Technology ${technology?counter}") is not None:
    placeholder = bpy.data.objects["Technology ${technology?counter}"]
    bpy.ops.wm.append(directory="${renderedConcepts[technology.getKey()].getAbsolutePath()}\\Collection\\", filename="Logo")
    logo = bpy.data.collections["Logo"]
    logo.name = "${technology.key} logo"
    logomesh = logo.objects.values()[0]
    logomesh.name = "${technology.key} logo"
    logomesh.parent = placeholder
</#list>


