import bpy

<#include "cleanup.ftl">

bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="Business Capability")

topTitle = bpy.data.objects["Business Capability Title"]
topTitle.data.body = "${element.title}"

bpy.ops.wm.save_as_mainfile(filepath="${targetFile}")