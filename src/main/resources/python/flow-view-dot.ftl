digraph G {
    graph [pad="0.1", nodesep="2", ranksep="1"];
    node[shape = square];

<#list view.elements as element>
    ${element.key} [width=2,height=2,fixedsize=true,id=${element.key},label="placeholder"]
</#list>

<#list view.steps as step>
    ${step.relationship.source.key} -> ${step.relationship.target.key}
</#list>

}