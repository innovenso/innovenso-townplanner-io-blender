import bpy

<#include "cleanup.ftl">

bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="Entity")

topTitle = bpy.data.objects["Video Title"]
topTitle.data.body = "${element.title}"

description = bpy.data.objects["Video Description"]
description.data.body = "${element.description}"

bpy.ops.wm.save_as_mainfile(filepath="${targetFile}")