import bpy

<#include "cleanup.ftl">

bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="Cameras", active_collection=False)
bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="Scenery", active_collection=False)
bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="View", active_collection=False)

topTitle = bpy.data.objects["View Title"]
topTitle.data.body = "${view.title}"

description = bpy.data.objects["View Description"]
description.data.body = "${view.description}"

systemCollection=bpy.data.collections["Systems"]

<#list view.systems as system>
bpy.ops.wm.link(directory="${system.getOutputFile("blender", "blender", "element")}\\Collection\\", filename="System", relative_path=True, active_collection=False)
system=bpy.data.objects["System"]
system.name="${system.key}"
system.location=(${view.getX(system.key)},${view.getY(system.key)},0)
systemCollection.objects.link(system)
bpy.context.scene.collection.objects.unlink(system)
</#list>

systemContextCollection=bpy.data.collections["System Contexts"]

<#list view.systemContexts as system>
bpy.ops.wm.link(directory="${system.getOutputFile("blender", "blender", "element")}\\Collection\\", filename="System Context", relative_path=True, active_collection=False)
system=bpy.data.objects["System Context"]
system.name="${system.key}"
system.location=(${view.getX(system.key)},${view.getY(system.key)},0)
systemContextCollection.objects.link(system)
bpy.context.scene.collection.objects.unlink(system)
</#list>

containerCollection=bpy.data.collections["Containers"]

<#list view.containers as container>
bpy.ops.wm.link(directory="${container.getOutputFile("blender", "blender", "element")}\\Collection\\", filename="Container", relative_path=True, active_collection=False)
container=bpy.data.objects["Container"]
container.name="${container.key}"
container.location=(${view.getX(container.key)},${view.getY(container.key)},0)
containerCollection.objects.link(container)
bpy.context.scene.collection.objects.unlink(container)
</#list>

actorCollection=bpy.data.collections["Actors"]

<#list view.actors as actor>
bpy.ops.wm.link(directory="${actor.getOutputFile("blender", "blender", "element")}\\Collection\\", filename="User", relative_path=True, active_collection=False)
actor=bpy.data.objects["User"]
actor.name="${actor.key}"
actor.location=(${view.getX(actor.key)},${view.getY(actor.key)},0)
actorCollection.objects.link(actor)
bpy.context.scene.collection.objects.unlink(actor)
</#list>

connectionCollection=bpy.data.collections["Relationships"]

<#list view.steps as step>
bpy.ops.wm.append(directory="${step.relationship.getOutputFile("blender", "blender", "element")}\\Collection\\", filename="Connection", active_collection=False)
connection=bpy.data.objects["Connection"]
connectionGroup=bpy.data.collections["Connection"]
connectionLine=bpy.data.objects["Connection Line"]
connectionOrigin=bpy.data.objects["Connection Origin"]
connectionTarget=bpy.data.objects["Connection Target"]
connectionTitle=bpy.data.objects["Connection Title"]
source=bpy.data.objects["${step.relationship.source.key}"]
target=bpy.data.objects["${step.relationship.target.key}"]
connectionTitle.data.body="${step?counter}. ${step.actualTitle}"
connectionOrigin.parent=source
connectionTarget.parent=target
connection.name="${step.relationship.key}"
connectionGroup.name="${step.relationship.key}"
connectionLine.name="${step.relationship.key} Line"
connectionOrigin.name="${step.relationship.key} Origin"
connectionTarget.name="${step.relationship.key} Target"
connectionTitle.name="${step.relationship.key} Title"
connection.location=(${step?index*3},-30,0)
connectionCollection.children.link(connectionGroup)
bpy.context.scene.collection.children.unlink(connectionGroup)
</#list>

bpy.ops.wm.save_as_mainfile(filepath="${targetFile}")