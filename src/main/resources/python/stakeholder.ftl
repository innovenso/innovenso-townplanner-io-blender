import bpy

<#include "cleanup.ftl">

bpy.ops.wm.append(directory="${templateFile}\\Collection\\", filename="User")

topTitle = bpy.data.objects["User Title Top"]
topTitle.data.body = "${element.title}"

sideTitle = bpy.data.objects["User Title Side"]
sideTitle.data.body = "${element.title}"

bpy.ops.wm.save_as_mainfile(filepath="${targetFile}")