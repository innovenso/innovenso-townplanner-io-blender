package com.innovenso.townplan.writer.pipeline.blender;

import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.UUID;

@Log4j2
public class BlenderTemplateFileProvider {

	public Optional<File> getBlenderTemplateFile(@NonNull String templateName, @NonNull String defaultTemplateName) {
		try {
			final String sourceBlenderFileName = templateName + ".blend";
			final String sourceDefaultBlenderFileName = defaultTemplateName + ".blend";
			log.info("template resource {}", sourceBlenderFileName);
			final File outputFile = File.createTempFile(UUID.randomUUID().toString(), ".blend");
			final InputStream templateInputStream = getClass().getResourceAsStream("/blender/" + sourceBlenderFileName);
			final InputStream defaultTemplateInputStream = getClass()
					.getResourceAsStream("/blender/" + sourceDefaultBlenderFileName);
			if (templateInputStream != null) {
				log.info("creating Blender template file {}", outputFile.getAbsolutePath());
				FileUtils.copyInputStreamToFile(templateInputStream, outputFile);
				log.info("Blender template file created");
			} else if (defaultTemplateInputStream != null) {
				log.info("creating default Blender template file");
				FileUtils.copyInputStreamToFile(defaultTemplateInputStream, outputFile);
			} else {
				log.info("no templates found");
				return Optional.empty();
			}
			return Optional.of(outputFile);
		} catch (IOException e) {
			log.error("Can not fetch Blender template file for template {}", templateName);
			return Optional.empty();
		}
	}
}
