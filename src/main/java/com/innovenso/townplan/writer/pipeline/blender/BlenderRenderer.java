package com.innovenso.townplan.writer.pipeline.blender;

import com.innovenso.townplan.writer.util.StreamGobbler;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.SystemUtils;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.Executors;

@Log4j2
public class BlenderRenderer {

	public Optional<File> render(@NonNull final File pythonScriptFile, @NonNull final File targetFile) {
		try {
			final Process process = new ProcessBuilder().command(getBlenderExecutablePath(), "--background",
					"--log-level", "-1", "--python", pythonScriptFile.getAbsolutePath())
					.directory(targetFile.getParentFile()).start();
			final StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), log::info);
			Executors.newSingleThreadExecutor().submit(streamGobbler);
			final int exitCode = process.waitFor();
			if (exitCode != 0) {
				log.error("Blender unsuccesful");
				return Optional.empty();
			} else {
				return Optional.of(targetFile);
			}
		} catch (IOException | InterruptedException e) {
			log.error(e);
			return Optional.empty();
		}
	}

	private String getBlenderExecutablePath() {
		if (SystemUtils.IS_OS_MAC)
			return "/Applications/Blender.app/Contents/MacOS/blender";
		else if (SystemUtils.IS_OS_LINUX)
			return "/usr/bin/blender";
		else
			return "blender";
	}
}
