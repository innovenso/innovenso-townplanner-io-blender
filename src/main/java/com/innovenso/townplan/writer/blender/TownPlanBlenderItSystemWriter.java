package com.innovenso.townplan.writer.blender;

import com.innovenso.townplan.api.value.it.ItSystem;
import com.innovenso.townplan.repository.AssetRepository;
import lombok.NonNull;

import java.io.File;

public class TownPlanBlenderItSystemWriter extends AbstractTownPlanBlenderConceptWriter<ItSystem> {
	public TownPlanBlenderItSystemWriter(@NonNull final AssetRepository assetRepository,
			@NonNull File targetBaseDirectory) {
		super(assetRepository, new File(targetBaseDirectory, "system"));
	}
}
