package com.innovenso.townplan.writer.blender;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.aspects.ContentOutputType;
import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.api.value.it.*;
import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.pipeline.AbstractTownPlanWriter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
public class TownPlanBlenderWriter extends AbstractTownPlanWriter {
	private final File blenderTargetDirectory;
	private final File baseTargetDirectory;
	private final TownPlanBlenderEnterpriseWriter enterpriseWriter;
	private final TownPlanBlenderBusinessCapabilityWriter capabilityWriter;
	private final TownPlanBlenderBuildingBlockWriter buildingBlockWriter;
	private final TownPlanBlenderBusinessActorWriter actorWriter;
	private final TownPlanBlenderEntityWriter entityWriter;
	private final TownPlanBlenderItSystemWriter systemWriter;
	private final TownPlanBlenderTechnologyWriter technologyWriter;
	private final TownPlanBlenderContainerWriter containerWriter;
	private final TownPlanBlenderRelationshipWriter relationshipWriter;

	public TownPlanBlenderWriter(@NonNull final String targetBasePath, @NonNull final AssetRepository assetRepository) {
		super(assetRepository, ContentOutputType.BLENDER);
		this.blenderTargetDirectory = new File(targetBasePath, "blender");
		this.baseTargetDirectory = new File(targetBasePath);
		this.blenderTargetDirectory.mkdirs();
		this.enterpriseWriter = new TownPlanBlenderEnterpriseWriter(assetRepository, this.blenderTargetDirectory);
		this.capabilityWriter = new TownPlanBlenderBusinessCapabilityWriter(assetRepository,
				this.blenderTargetDirectory);
		this.buildingBlockWriter = new TownPlanBlenderBuildingBlockWriter(assetRepository, this.blenderTargetDirectory);
		this.actorWriter = new TownPlanBlenderBusinessActorWriter(assetRepository, this.blenderTargetDirectory);
		this.entityWriter = new TownPlanBlenderEntityWriter(assetRepository, this.blenderTargetDirectory);
		this.systemWriter = new TownPlanBlenderItSystemWriter(assetRepository, this.blenderTargetDirectory);
		this.technologyWriter = new TownPlanBlenderTechnologyWriter(assetRepository, this.blenderTargetDirectory);
		this.containerWriter = new TownPlanBlenderContainerWriter(assetRepository, this.blenderTargetDirectory);
		this.relationshipWriter = new TownPlanBlenderRelationshipWriter(assetRepository, this.blenderTargetDirectory);
	}

	@Override
	public void writeFiles(TownPlan townPlan, List<File> renderedFiles) {
		final Map<String, File> renderedConcepts = new HashMap<>();
		townPlan.getElements(Enterprise.class)
				.forEach(enterprise -> enterpriseWriter.write(enterprise, renderedConcepts, townPlan));
		townPlan.getElements(BusinessCapability.class)
				.forEach(capability -> capabilityWriter.write(capability, renderedConcepts, townPlan));
		townPlan.getElements(ArchitectureBuildingBlock.class)
				.forEach(block -> buildingBlockWriter.write(block, renderedConcepts, townPlan));
		townPlan.getElements(BusinessActor.class)
				.forEach(actor -> actorWriter.write(actor, renderedConcepts, townPlan));
		townPlan.getElements(DataEntity.class)
				.forEach(dataEntity -> entityWriter.write(dataEntity, renderedConcepts, townPlan));
		townPlan.getElements(ItSystem.class).forEach(system -> systemWriter.write(system, renderedConcepts, townPlan));
		townPlan.getElements(Technology.class)
				.forEach(technology -> technologyWriter.write(technology, renderedConcepts, townPlan));
		townPlan.getElements(ItContainer.class)
				.forEach(itContainer -> containerWriter.write(itContainer, renderedConcepts, townPlan));
		townPlan.getAllRelationships()
				.forEach(relationship -> relationshipWriter.write(relationship, renderedConcepts, townPlan));
		renderedFiles.addAll(renderedConcepts.values());
		renderedFiles
				.forEach(renderedFile -> getAssetRepository().write(renderedFile, getAssetObjectName(renderedFile)));
	}

	@Override
	protected void writeFiles(@NonNull TownPlan townPlan, @NonNull List<File> renderedFiles, @NonNull String s) {
		final Map<String, File> renderedConcepts = new HashMap<>();
		townPlan.getConcept(s).ifPresent(concept -> {
			if (concept instanceof Enterprise enterprise) {
				enterpriseWriter.write(enterprise, renderedConcepts, townPlan);
			} else if (concept instanceof BusinessCapability capability) {
				capabilityWriter.write(capability, renderedConcepts, townPlan);
			} else if (concept instanceof ArchitectureBuildingBlock buildingBlock) {
				buildingBlockWriter.write(buildingBlock, renderedConcepts, townPlan);
			} else if (concept instanceof BusinessActor actor) {
				actorWriter.write(actor, renderedConcepts, townPlan);
			} else if (concept instanceof DataEntity entity) {
				entityWriter.write(entity, renderedConcepts, townPlan);
			} else if (concept instanceof ItSystem system) {
				systemWriter.write(system, renderedConcepts, townPlan);
			} else if (concept instanceof Technology technology) {
				technologyWriter.write(technology, renderedConcepts, townPlan);
			} else if (concept instanceof ItContainer container) {
				container.getTechnologies()
						.forEach(technology -> technologyWriter.write(technology, renderedConcepts, townPlan));
				containerWriter.write(container, renderedConcepts, townPlan);
			}
		});
		townPlan.getAllRelationships().stream()
				.filter(relationship -> relationship.getSource().getKey().equals(s)
						|| relationship.getTarget().getKey().equals(s))
				.forEach(relationship -> relationshipWriter.write(relationship, renderedConcepts, townPlan));

		renderedFiles.addAll(renderedConcepts.values());
		renderedFiles
				.forEach(renderedFile -> getAssetRepository().write(renderedFile, getAssetObjectName(renderedFile)));
	}

	@Override
	public File getTargetBaseDirectory() {
		return blenderTargetDirectory;
	}

	@Override
	public void setTargetBaseDirectory(@NonNull final File targetBaseDirectory) {
	}

	private String getAssetObjectName(@NonNull final File renderedFile) {
		log.info("base directory: {}", baseTargetDirectory.getAbsolutePath());
		log.info("rendered file: {}", renderedFile.getAbsolutePath());
		final String relativePath = baseTargetDirectory.toPath().toAbsolutePath()
				.relativize(renderedFile.toPath().toAbsolutePath()).toString();
		log.info("asset object name: {}", relativePath);
		return relativePath;
	}
}
