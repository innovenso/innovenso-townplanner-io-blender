package com.innovenso.townplan.writer.blender;

import com.innovenso.townplan.api.value.business.BusinessActor;
import com.innovenso.townplan.repository.AssetRepository;
import lombok.NonNull;

import java.io.File;

public class TownPlanBlenderBusinessActorWriter extends AbstractTownPlanBlenderConceptWriter<BusinessActor> {
	public TownPlanBlenderBusinessActorWriter(@NonNull final AssetRepository assetRepository,
			@NonNull File targetBaseDirectory) {
		super(assetRepository, new File(targetBaseDirectory, "business_actor"));
	}

	@Override
	public String getBlenderTemplateName(BusinessActor concept) {
		return "user";
	}

	@Override
	public String getBlenderPythonScriptTemplateName(BusinessActor concept) {
		return "user";
	}
}
