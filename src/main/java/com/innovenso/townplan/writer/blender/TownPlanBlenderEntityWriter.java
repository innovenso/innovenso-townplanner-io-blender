package com.innovenso.townplan.writer.blender;

import com.innovenso.townplan.api.value.it.DataEntity;
import com.innovenso.townplan.repository.AssetRepository;
import lombok.NonNull;

import java.io.File;

public class TownPlanBlenderEntityWriter extends AbstractTownPlanBlenderConceptWriter<DataEntity> {
	public TownPlanBlenderEntityWriter(@NonNull final AssetRepository assetRepository,
			@NonNull File targetBaseDirectory) {
		super(assetRepository, new File(targetBaseDirectory, "entity"));
	}
}
