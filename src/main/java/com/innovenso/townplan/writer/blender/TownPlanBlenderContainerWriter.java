package com.innovenso.townplan.writer.blender;

import com.innovenso.townplan.api.value.it.ItContainer;
import com.innovenso.townplan.repository.AssetRepository;
import lombok.NonNull;

import java.io.File;

public class TownPlanBlenderContainerWriter extends AbstractTownPlanBlenderConceptWriter<ItContainer> {
	public TownPlanBlenderContainerWriter(@NonNull final AssetRepository assetRepository,
			@NonNull File targetBaseDirectory) {
		super(assetRepository, new File(targetBaseDirectory, "container"));
	}
}
