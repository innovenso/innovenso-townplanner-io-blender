package com.innovenso.townplan.writer.blender;

import com.innovenso.townplan.api.TownPlan;
import com.innovenso.townplan.api.value.Concept;
import com.innovenso.townplan.domain.KeyPointInTimeImpl;
import com.innovenso.townplan.repository.AssetRepository;
import com.innovenso.townplan.writer.model.ConceptWriterModel;
import com.innovenso.townplan.writer.pipeline.blender.BlenderRenderer;
import com.innovenso.townplan.writer.pipeline.blender.BlenderTemplateFileProvider;
import com.innovenso.townplan.writer.pipeline.freemarker.FreemarkerRenderer;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Log4j2
public abstract class AbstractTownPlanBlenderConceptWriter<T extends Concept> {
	private final FreemarkerRenderer freemarkerRenderer;
	private final BlenderTemplateFileProvider blenderTemplateFileProvider;
	private final BlenderRenderer blenderRenderer;
	private final File targetBaseDirectory;
	private final AssetRepository assetRepository;

	public AbstractTownPlanBlenderConceptWriter(@NonNull final AssetRepository assetRepository,
			@NonNull final File targetBaseDirectory) {
		this.freemarkerRenderer = new FreemarkerRenderer("python");
		this.blenderTemplateFileProvider = new BlenderTemplateFileProvider();
		this.blenderRenderer = new BlenderRenderer();
		this.targetBaseDirectory = targetBaseDirectory;
		this.assetRepository = assetRepository;
		if (!targetBaseDirectory.exists())
			targetBaseDirectory.mkdirs();
	}

	private Optional<File> getBlenderPythonScriptFile(final File targetBlenderFile, final T element,
			final Map<String, File> renderedConceptMap, @NonNull final TownPlan townPlan) {
		log.info("get Blender Python Script file for element {}", element.getKey());
		final Optional<File> inputBlenderFile = blenderTemplateFileProvider
				.getBlenderTemplateFile(getBlenderTemplateName(element), getDefaultBlenderTemplateName());
		final Optional<Map<String, Object>> inputModel = inputBlenderFile.map(template -> Map.of("element",
				(Object) new ConceptWriterModel<>(element, KeyPointInTimeImpl.builder().date(LocalDate.now()).build(),
						townPlan),
				"templateFile", template.getAbsolutePath(), "targetFile", targetBlenderFile.getAbsolutePath(),
				"renderedConcepts", (Object) renderedConceptMap));
		return inputModel
				.map(model -> freemarkerRenderer.write(model, getBlenderPythonScriptTemplateName(element) + ".ftl"))
				.filter(Optional::isPresent).map(Optional::get);
	}

	Optional<File> write(T element, final Map<String, File> renderedConceptMap, @NonNull final TownPlan townPlan) {
		try {
			log.info("rendering {}", element);
			final File finalBlenderOutputFile = File.createTempFile(element.getKey(), UUID.randomUUID().toString());
			final Optional<File> pythonScriptFile = getBlenderPythonScriptFile(finalBlenderOutputFile, element,
					renderedConceptMap, townPlan);
			final Optional<File> renderedFile = pythonScriptFile
					.map(python -> blenderRenderer.render(python, finalBlenderOutputFile)).map(Optional::get);
			return renderedFile.map(render -> {
				final File resultingFile = new File(targetBaseDirectory, element.getKey() + ".blend");
				try {
					FileUtils.copyFile(render, resultingFile);
					log.info("Blender output file saved {}", resultingFile.getAbsolutePath());
					renderedConceptMap.put(element.getKey(), resultingFile);
				} catch (IOException e) {
					return null;
				}
				return resultingFile;
			});
		} catch (IOException e) {
			log.error(e);
			return Optional.empty();
		}
	}

	protected String getBlenderPythonScriptTemplateName(T concept) {
		return concept.getType().toLowerCase();
	}

	protected String getBlenderTemplateName(T concept) {
		return concept.getType().toLowerCase();
	}

	protected String getDefaultBlenderTemplateName() {
		return "element";
	}
}
