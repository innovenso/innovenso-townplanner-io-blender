package com.innovenso.townplan.writer.blender;

import com.innovenso.townplan.api.value.strategy.BusinessCapability;
import com.innovenso.townplan.repository.AssetRepository;
import lombok.NonNull;

import java.io.File;

public class TownPlanBlenderBusinessCapabilityWriter extends AbstractTownPlanBlenderConceptWriter<BusinessCapability> {
	public TownPlanBlenderBusinessCapabilityWriter(@NonNull final AssetRepository assetRepository,
			@NonNull File targetBaseDirectory) {
		super(assetRepository, new File(targetBaseDirectory, "capability"));
	}

	@Override
	public String getBlenderTemplateName(@NonNull final BusinessCapability concept) {
		int templateLevel = concept.getLevel() > 1 ? 2 : concept.getLevel();
		return "business-capability-" + templateLevel;
	}

	@Override
	public String getBlenderPythonScriptTemplateName(BusinessCapability concept) {
		int templateLevel = concept.getLevel() > 1 ? 2 : concept.getLevel();
		return "business-capability-" + templateLevel;
	}
}
