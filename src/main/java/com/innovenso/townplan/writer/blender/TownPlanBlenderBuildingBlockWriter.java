package com.innovenso.townplan.writer.blender;

import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock;
import com.innovenso.townplan.repository.AssetRepository;
import lombok.NonNull;

import java.io.File;

public class TownPlanBlenderBuildingBlockWriter
		extends
			AbstractTownPlanBlenderConceptWriter<ArchitectureBuildingBlock> {
	public TownPlanBlenderBuildingBlockWriter(@NonNull final AssetRepository assetRepository,
			@NonNull File targetBaseDirectory) {
		super(assetRepository, new File(targetBaseDirectory, "building_blocks"));
	}

	@Override
	public String getBlenderTemplateName(ArchitectureBuildingBlock concept) {
		return "building-block";
	}

	@Override
	public String getBlenderPythonScriptTemplateName(ArchitectureBuildingBlock concept) {
		return "building-block";
	}
}
