package com.innovenso.townplan.writer.blender;

import com.innovenso.townplan.api.value.it.Technology;
import com.innovenso.townplan.repository.AssetRepository;
import lombok.NonNull;

import java.io.File;

public class TownPlanBlenderTechnologyWriter extends AbstractTownPlanBlenderConceptWriter<Technology> {
	public TownPlanBlenderTechnologyWriter(@NonNull final AssetRepository assetRepository,
			@NonNull File targetBaseDirectory) {
		super(assetRepository, new File(targetBaseDirectory, "technology"));
	}

	@Override
	public String getBlenderTemplateName(Technology concept) {
		return concept.getKey();
	}

	@Override
	protected String getDefaultBlenderTemplateName() {
		return "technology-unknown";
	}

	@Override
	public String getBlenderPythonScriptTemplateName(Technology concept) {
		return "logo";
	}
}
