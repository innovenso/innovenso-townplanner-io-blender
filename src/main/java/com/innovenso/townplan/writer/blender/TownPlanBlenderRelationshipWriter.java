package com.innovenso.townplan.writer.blender;

import com.innovenso.townplan.api.value.Relationship;
import com.innovenso.townplan.repository.AssetRepository;
import lombok.NonNull;

import java.io.File;

public class TownPlanBlenderRelationshipWriter extends AbstractTownPlanBlenderConceptWriter<Relationship> {
	public TownPlanBlenderRelationshipWriter(@NonNull final AssetRepository assetRepository,
			@NonNull File targetBaseDirectory) {
		super(assetRepository, new File(targetBaseDirectory, "relationship"));
	}

	@Override
	public String getBlenderTemplateName(Relationship element) {
		return element.isBidirectional() ? "connection-bidirectional" : "connection-unidirectional";
	}

	@Override
	public String getBlenderPythonScriptTemplateName(Relationship element) {
		return element.isBidirectional() ? "connection-bidirectional" : "connection-unidirectional";
	}
}
