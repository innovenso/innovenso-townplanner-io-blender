package com.innovenso.townplan.writer.blender;

import com.innovenso.townplan.api.value.business.Enterprise;
import com.innovenso.townplan.repository.AssetRepository;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.File;

@Log4j2
public class TownPlanBlenderEnterpriseWriter extends AbstractTownPlanBlenderConceptWriter<Enterprise> {
	public TownPlanBlenderEnterpriseWriter(@NonNull final AssetRepository assetRepository,
			@NonNull File targetBaseDirectory) {
		super(assetRepository, new File(targetBaseDirectory, "enterprise"));
	}
}
