package com.innovenso.townplan.writer.blender

import com.innovenso.eventsourcing.api.id.EntityId
import com.innovenso.townplan.api.value.Relationship
import com.innovenso.townplan.api.value.RelationshipType
import com.innovenso.townplan.api.value.business.BusinessActor
import com.innovenso.townplan.api.value.business.Enterprise
import com.innovenso.townplan.api.value.enums.TechnologyRecommendation
import com.innovenso.townplan.api.value.it.ArchitectureBuildingBlock
import com.innovenso.townplan.api.value.it.DataEntity
import com.innovenso.townplan.api.value.it.ItSystem
import com.innovenso.townplan.api.value.it.Technology
import com.innovenso.townplan.api.value.strategy.BusinessCapability
import com.innovenso.townplan.domain.*
import com.innovenso.townplan.domain.sample.SampleFactory
import com.innovenso.townplan.repository.FileSystemAssetRepository
import spock.lang.Ignore
import spock.lang.Specification

import java.nio.file.Files

@Ignore
class TownPlanBlenderWriterSpec extends Specification {
	def writePath = com.google.common.io.Files.createTempDir().getAbsolutePath()
	def assetRepository = new FileSystemAssetRepository(writePath, "https://test.com")
	def townPlan = new TownPlanImpl(new EntityId("test"))
	def samples = new SampleFactory(townPlan)
	Map<String,File> renderedConcepts = [:]

	def "town plan is written to zip file"() {
		given:
		samples.enterprise()
		TownPlanBlenderWriter writer = new TownPlanBlenderWriter(Files.createTempDirectory("spock").toFile().getAbsolutePath(), assetRepository)
		when:
		writer.write(townPlan)
		then:
		assetRepository.getObjectNames()
		assetRepository.getObjectNames().each {println it}
	}

	def "enterprise is written to a Blender file"() {
		given:
		TownPlanBlenderEnterpriseWriter writer = new TownPlanBlenderEnterpriseWriter(assetRepository, Files.createTempDirectory("spock").toFile())
		def enterprise = samples.enterprise()
		when:
		Optional<File> output = writer.write(enterprise, renderedConcepts, townPlan)
		then:
		output.isPresent()
		output.get().exists()
		println output.get().getAbsolutePath()
	}

	def "business capability is written to a Blender file"() {
		given:
		TownPlanBlenderBusinessCapabilityWriter writer = new TownPlanBlenderBusinessCapabilityWriter(assetRepository, Files.createTempDirectory("spock").toFile())
		def enterprise = samples.enterprise()
		def capability = samples.capability(enterprise)
		when:
		Optional<File> output = writer.write(capability, renderedConcepts, townPlan)
		then:
		output.isPresent()
		output.get().exists()
		println output.get().getAbsolutePath()
	}

	def "building block is written to a Blender file"() {
		given:
		TownPlanBlenderBuildingBlockWriter writer = new TownPlanBlenderBuildingBlockWriter(assetRepository, Files.createTempDirectory("spock").toFile())
		def enterprise = samples.enterprise()
		def capability = samples.capability(enterprise)
		def block = samples.buildingBlock(enterprise, capability)
		when:
		Optional<File> output = writer.write(block, renderedConcepts, townPlan)
		then:
		output.isPresent()
		output.get().exists()
		println output.get().getAbsolutePath()
	}

	def "business actor is written to a Blender file"() {
		given:
		TownPlanBlenderBusinessActorWriter writer = new TownPlanBlenderBusinessActorWriter(assetRepository, Files.createTempDirectory("spock").toFile())
		def enterprise = samples.enterprise()
		def actor = samples.actor(enterprise)
		when:
		Optional<File> output = writer.write(actor, renderedConcepts, townPlan)
		then:
		output.isPresent()
		output.get().exists()
		println output.get().getAbsolutePath()
	}

	def "data entity is written to a Blender file"() {
		given:
		TownPlanBlenderEntityWriter writer = new TownPlanBlenderEntityWriter(assetRepository, Files.createTempDirectory("spock").toFile())
		def enterprise = samples.enterprise()
		def entity = samples.entity(enterprise.key)
		when:
		Optional<File> output = writer.write(entity, renderedConcepts, townPlan)
		then:
		output.isPresent()
		output.get().exists()
		println output.get().getAbsolutePath()
	}

	def "IT System is written to a Blender file"() {
		given:
		TownPlanBlenderItSystemWriter writer = new TownPlanBlenderItSystemWriter(assetRepository, Files.createTempDirectory("spock").toFile())
		def system = samples.system()
		when:
		Optional<File> output = writer.write(system, renderedConcepts, townPlan)
		then:
		output.isPresent()
		output.get().exists()
		println output.get().getAbsolutePath()
	}

	def "technology is written to a Blender file"() {
		given:
		TownPlanBlenderTechnologyWriter writer = new TownPlanBlenderTechnologyWriter(assetRepository, Files.createTempDirectory("spock").toFile())
		def technology = samples.technology()
		when:
		Optional<File> output = writer.write(technology, renderedConcepts, townPlan)
		then:
		output.isPresent()
		output.get().exists()
		println output.get().getAbsolutePath()
	}

	def "IT Container is written to a Blender file"() {
		given:
		File targetDirectory = Files.createTempDirectory("spock").toFile()
		TownPlanBlenderTechnologyWriter technologyWriter = new TownPlanBlenderTechnologyWriter(assetRepository, targetDirectory)
		TownPlanBlenderContainerWriter containerWriter = new TownPlanBlenderContainerWriter(assetRepository, targetDirectory)
		def system = samples.system()
		def container = samples.container(system)

		when:
		container.technologies.each {technologyWriter.write(it, renderedConcepts, townPlan)}
		Optional<File> containerOutput = containerWriter.write(container, renderedConcepts, townPlan)
		then:
		containerOutput.isPresent()
		containerOutput.get().exists()
		println containerOutput.get().getAbsolutePath()
	}

	def "Relationship is written to a Blender file"() {
		given:
		File targetDirectory = Files.createTempDirectory("spock").toFile()
		TownPlanBlenderRelationshipWriter relationshipWriter = new TownPlanBlenderRelationshipWriter(assetRepository, targetDirectory)
		TownPlanBlenderItSystemWriter systemWriter = new TownPlanBlenderItSystemWriter(assetRepository, targetDirectory)

		def system1 = samples.system()
		def system2 = samples.system()
		def relationship = samples.flow(system1, system2)


		when:
		Optional<File> systemOutput1 = systemWriter.write(system1, renderedConcepts, townPlan)
		Optional<File> systemOutput2 = systemWriter.write(system2, renderedConcepts, townPlan)
		Optional<File> relationshipOutput = relationshipWriter.write(relationship, renderedConcepts, townPlan)
		then:
		relationshipOutput.isPresent()
		relationshipOutput.get().exists()
		println relationshipOutput.get().getAbsolutePath()
	}
}
